import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';

import AuthScreen from './src/screens/Auth/Auth';
import SharePlaceScreen from './src/screens/SharePlace/SharePlace';
import FindPlaceScreen from './src/screens/FindPlace/FindPlace';
import PlaceDetailScreen from './src/screens/PlaceDetail/PlaceDetail';
import SideDrawer from './src/screens/SideDrawer/SideDrawer';

import configureStore from './src/store/configureStore';
import placeDetail from './src/screens/PlaceDetail/PlaceDetail';

const store = configureStore();

//register screens
Navigation.registerComponent("devApp.AuthScreen", () => AuthScreen, store, Provider);
Navigation.registerComponent("devApp.SharePlaceScreen", () => SharePlaceScreen, store, Provider);
Navigation.registerComponent("devApp.FindPlaceScreen", () => FindPlaceScreen, store, Provider);

Navigation.registerComponent("devApp.PlaceDetailScreen", () => PlaceDetailScreen, store, Provider )

Navigation.registerComponent("devApp.SideDrawer", () => SideDrawer)

//start an app
Navigation.startSingleScreenApp({
    screen: {
        screen: "devApp.AuthScreen",
        title: "Login"
    }
});

