import React from 'react';
import {Text, StyleSheet} from 'react-native';

const headingText = props => (
    <Text
        {...props}
        style={[styles.textHeading, {color: props.color}]}>
            {props.children}
    </Text>
);

const styles = StyleSheet.create({
    textHeading:  {
        fontSize: 28,
        fontWeight: "bold",
        color: "white"
    }

});

export default headingText;