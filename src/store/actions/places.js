import { ADD_PLACE, DELETE_PLACE } from './actionTypes.js';

// export const addPlace = (placeName, location, image) => {
//     // return {
//     //     type: ADD_PLACE,
//     //     placeName: placeName,
//     //     location: location,
//     //     image: image
//     // };
//     return dispatch => {
//         const placeData = {
//             name: placeName,
//             location: location
//         };
//         fetch("https://us-central1-react-app-1562105876934.cloudfunctions.net/storeImage", {
//             method: "POST",
//             body: JSON.stringify({
//                 image: image.base64
//             })
//         })
//         .catch(err => console.log(err))
//         .then(res => res.json())
//         .then(parsedRes => {
//             console.log(parsedRes);
//         });
//         // fetch("https://react-app-1562105876934.firebaseio.com/places.json", {
//         //     method: "POST",
//         //     body: JSON.stringify(placeData)
//         // })
//         // .catch(err => console.log(err))
//         // .then(res => res.json())
//         // .then(parsedRes => {
//         //     console.log(parsedRes);
//         // });
//     };
// };

export const addPlace = (placeName, location, image) => {
    return dispatch => {
        fetch("https://us-central1-react-app-1562105876934.cloudfunctions.net/storeImage", {
            method: "POST",
            body: JSON.stringify({
                image: image.base64
            })
        })
        .catch(err => console.log(err))
        .then(res => res.json())
        .then(parsedRes => {
            const placeData = {
                name: placeName,
                location: location,
                image: parsedRes.imageUrl
            };
            return fetch("https://react-app-1562105876934.firebaseio.com/places.json", {
                method: "POST",
                body: JSON.stringify(placeData)
            })
        })  
        .catch(err => console.log(err))
        .then(res => {
            res.json();
        })
        .then(parsedRes => {
            console.log(parsedRes);
        });
    };
};

export const deletePlace = (key) => {
    return {
        type: DELETE_PLACE,
        placeKey: key
    };
};

